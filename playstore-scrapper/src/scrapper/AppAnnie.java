package scrapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Scanner;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class AppAnnie {
	private String url;
	private String charset;
	private String token;
	private String packageName;
	
	public AppAnnie(String pn) {
		url = "https://api.appannie.com/v1.2";
		charset = java.nio.charset.StandardCharsets.UTF_8.name();
		token = getToken();
		packageName = pn;
	}
	
	public String getToken() {
		String token = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File("./data/appannie.txt")));
			token = "bearer " + br.readLine();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return token;
	}
	
	public String getQuery() {
		try {
			return String.format("access_token=%s&p=%s", 
				     URLEncoder.encode(token, charset), 
				     URLEncoder.encode(packageName, charset));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public String request() {
		URLConnection connection;
		try {
			System.out.println(url + "?" + getQuery());
			connection = new URL(url + "?" + getQuery()).openConnection();
			connection.setRequestProperty("Accept-Charset", charset);
			connection.setRequestProperty("User-Agent","Mozilla/5.0 ( compatible ) ");
			connection.setRequestProperty("Accept","*/*");
			InputStream response = connection.getInputStream();
			Scanner scanner = new Scanner(response);
		    return scanner.useDelimiter("\\A").next();
		} catch (IOException e) {
			return "{}";
		}
	}
	
	public JsonObject getReviews(String responseBody) {
		JsonObject reviews = new JsonParser().parse(responseBody).getAsJsonObject();
		return reviews;
	}
}
