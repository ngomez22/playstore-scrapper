package scrapper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import dtos.Review;

public class FortytwoMatters {
	private String url;
	private String charset;
	private String token;
	private String packageName;
	private String lang;
	
	public FortytwoMatters(String pn) {
		url = "https://data.42matters.com/api/v2.0/android/apps/reviews.json";
		lang = "en";
		charset = java.nio.charset.StandardCharsets.UTF_8.name();
		token = getToken();
		packageName = pn;
	}
	
	public String getToken() {
		String token = "";
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File("./data/42matters.txt")));
			token = br.readLine();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return token;
	}
	
	public String getQuery() {
		try {
			return String.format("access_token=%s&p=%s&lang=%s&limit=100", 
				     URLEncoder.encode(token, charset), 
				     URLEncoder.encode(packageName, charset),
				     URLEncoder.encode(lang, charset));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public String request() {
		URLConnection connection;
		try {
			connection = new URL(url + "?" + getQuery()).openConnection();
			connection.setRequestProperty("Accept-Charset", charset);
			connection.setRequestProperty("User-Agent","Mozilla/5.0 ( compatible ) ");
			connection.setRequestProperty("Accept","*/*");
			InputStream response = connection.getInputStream();
		    return new Scanner(response).useDelimiter("\\A").next();
		} catch (IOException e) {
			return "{}";
		}
	}
	
	public JsonObject responseToJson(String responseBody) {
		JsonObject reviews = new JsonParser().parse(responseBody).getAsJsonObject();
		return reviews;
	}
	
	public List<Review> getReviews() {
		String response = request();
		ArrayList<Review> answer = new ArrayList<>();
		JsonObject r = responseToJson(response);
		if (r.get("reviews") != null) {
			DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
			JsonArray reviews = r.getAsJsonArray("reviews");
			for(int i = 0; i < reviews.size(); i++) {
				JsonObject rev = (JsonObject) reviews.get(i);
				answer.add(new Review(
						rev.get("author_id").getAsString(), 
						Integer.parseInt(rev.get("rating").getAsString()),
						rev.get("content").getAsString(),
						formatter.parseDateTime(rev.get("date").getAsString())
					)
				);
			}
		}
		return answer;
	}
}
