package scrapper;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import dtos.App;
import dtos.Review;

public class PlayStore {

	public static final String URL = "https://play.google.com";
	public static final String CATEGORY_URL = "https://play.google.com/store/apps/category/";

	private List<App> apps;

	public PlayStore() {
		apps = new ArrayList<App>();
	}

	public List<App> getApps() {
		return apps;
	}
	
	public String getSection(int number) {
		switch(number) {
			case 1: 
				return "topselling_paid";
			case 2:
				return "topselling_free";					
			case 3: 
				return "topgrossing";
			default:
				return "movers_shakers";
		}
	}

	public void topAppsByCategory(String category, int section) throws IOException {
		String firstUrl = CATEGORY_URL + category + "/collection/" + getSection(section);
		Document doc = Jsoup.connect(firstUrl).timeout(0).get();
		Elements apps = doc.getElementsByClass("card-click-target");
		HashSet<String> hrefs = new HashSet<String>();
		for(Element app : apps) {
			hrefs.add(app.attr("href").toString());
		}
		for(String url : hrefs) {
			String packName = url.split("id=")[1];
			getAppInfo(URL + url, packName);
		}
		sortApps();
	}

	public void getAppInfo(String appUrl, String packName) throws IOException {
		Document details = Jsoup.connect(appUrl).timeout(0).get();
		String name = details.select(".id-app-title").text();
		String description = details.select("[itemprop=description]").text();
		String author = details.select("[itemprop=author]").text();
		String downloads = details.select("[itemprop=numDownloads]").text();
		String price = details.select(".price").text();
		double score = Double.parseDouble(details.select(".score").text().replaceAll(",", "."));
		int ratings = Integer.parseInt(details.select(".rating-count").text().replaceAll(",", ""));
		int fiveStars = Integer.parseInt(details.select(".rating-bar-container.five").text().split(" ")[1].replaceAll(",", ""));
		int fourStars = Integer.parseInt(details.select(".rating-bar-container.four").text().split(" ")[1].replaceAll(",", ""));
		int threeStars = Integer.parseInt(details.select(".rating-bar-container.three").text().split(" ")[1].replaceAll(",", ""));
		int twoStars = Integer.parseInt(details.select(".rating-bar-container.two").text().split(" ")[1].replaceAll(",", ""));
		int oneStar = Integer.parseInt(details.select(".rating-bar-container.one").text().split(" ")[1].replaceAll(",", ""));
		String newFeatures = details.select(".details-section.whatsnew").toString();
		App app = new App(packName, name, author, ratings, score, price, description, downloads, fiveStars, fourStars, threeStars, twoStars, oneStar, newFeatures);
		getAppReviews(app);
		apps.add(app);
		System.out.println(app.getTitle() + ". " + app.getReviews().size() + " reviews");
	}

	public void getAppReviews(App app) {
		FortytwoMatters r = new FortytwoMatters(app.getPackageName());
		List<Review> reviews = r.getReviews();
		app.addReviews(reviews);
	}

	public void sortApps() {
		Collections.sort(apps);
		Collections.reverse(apps);
	}

	public void printApps() {
		for(App a : apps) {
			System.out.println(a.toString());
			System.out.println("-- Reviews:");
			for(Review r : a.getReviews()) {
				System.out.println(r.toString());
			}
		}
	}

	public void output(String fileName) throws IOException {
		Workbook wb = new HSSFWorkbook();
		CreationHelper createHelper = wb.getCreationHelper();
		Sheet main = wb.createSheet("apps");
		Row row = main.createRow((short)0);
		row.createCell(0).setCellValue(createHelper.createRichTextString("Title"));
		row.createCell(1).setCellValue(createHelper.createRichTextString("Package name"));
		row.createCell(2).setCellValue(createHelper.createRichTextString("Developer"));
		row.createCell(3).setCellValue(createHelper.createRichTextString("Score"));
		row.createCell(4).setCellValue(createHelper.createRichTextString("Number of ratings"));
		row.createCell(5).setCellValue(createHelper.createRichTextString("5 star ratings"));
		row.createCell(6).setCellValue(createHelper.createRichTextString("4 star ratings"));
		row.createCell(7).setCellValue(createHelper.createRichTextString("3 star ratings"));
		row.createCell(8).setCellValue(createHelper.createRichTextString("2 star ratings"));
		row.createCell(9).setCellValue(createHelper.createRichTextString("1 star ratings"));
		row.createCell(10).setCellValue(createHelper.createRichTextString("Price"));
		row.createCell(11).setCellValue(createHelper.createRichTextString("Description"));
		row.createCell(12).setCellValue(createHelper.createRichTextString("New features"));
		int r = 1;
		for(App a : apps) {
			Row newRow = main.createRow((short) r++);
			newRow.createCell(0).setCellValue(createHelper.createRichTextString(a.getTitle()));
			newRow.createCell(1).setCellValue(createHelper.createRichTextString(a.getPackageName()));
			newRow.createCell(2).setCellValue(createHelper.createRichTextString(a.getDeveloper()));
			newRow.createCell(3).setCellValue(a.getScore());
			newRow.createCell(4).setCellValue(a.getRatings());
			newRow.createCell(5).setCellValue(a.getFiveStars());
			newRow.createCell(6).setCellValue(a.getFourStars());
			newRow.createCell(7).setCellValue(a.getThreeStars());
			newRow.createCell(8).setCellValue(a.getTwoStars());
			newRow.createCell(9).setCellValue(a.getOneStar());
			newRow.createCell(10).setCellValue(createHelper.createRichTextString(a.getPrice()));
			newRow.createCell(11).setCellValue(createHelper.createRichTextString(a.getDescription()));
			newRow.createCell(12).setCellValue(createHelper.createRichTextString(a.getNewFeatures()));
			Sheet reviews = wb.createSheet(WorkbookUtil.createSafeSheetName(
					(a.getTitle().length() > 31 ? a.getTitle().substring(0, 31) : a.getTitle()).replaceAll(":", " ")
			));
			Row reviewHeader = reviews.createRow((short)0);
			reviewHeader.createCell(0).setCellValue(createHelper.createRichTextString("Author"));
			reviewHeader.createCell(1).setCellValue(createHelper.createRichTextString("Rating"));
			reviewHeader.createCell(2).setCellValue(createHelper.createRichTextString("Content"));
			reviewHeader.createCell(3).setCellValue(createHelper.createRichTextString("Date"));
			int rr = 1;
			for(Review review : a.getReviews()) {
				Row reviewRow = reviews.createRow(rr++);
				reviewRow.createCell(0).setCellValue(createHelper.createRichTextString(review.getAuthor()));
				reviewRow.createCell(1).setCellValue(review.getRating());
				reviewRow.createCell(2).setCellValue(createHelper.createRichTextString(review.getContent()));
				reviewRow.createCell(3).setCellValue(createHelper.createRichTextString(review.getDate().toString()));
			}
		}
		// Write the output to a file
		FileOutputStream fileOut = new FileOutputStream("data/" + fileName + ".xls");
		wb.write(fileOut);
		fileOut.close();
	}
}
