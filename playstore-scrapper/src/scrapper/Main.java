package scrapper;

import java.io.IOException;
import java.util.Scanner;

public class Main {
	
	private static PlayStore ps;
	
	public static void main(String[] args) throws IOException {
		ps = new PlayStore();
		Scanner reader = new Scanner(System.in);  // Reading from System.in
		System.out.println("Category to search: ");
		String category = reader.nextLine(); 
		System.out.println("Filter:");
		System.out.println("1: Top selling paid");
		System.out.println("2: Top selling free");
		System.out.println("3: Top grossing");
		System.out.println("4: Trending");
		int section = Integer.parseInt(reader.nextLine());
		System.out.println("Enter a name for the file containing your results:");
		String fileName = reader.nextLine();
		ps.topAppsByCategory(category.isEmpty() ? "LIFESTYLE" : category, section);
		ps.printApps();
		ps.output(fileName.isEmpty() ? "results" : fileName);
	}
}