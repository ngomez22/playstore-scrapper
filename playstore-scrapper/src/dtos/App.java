package dtos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App implements Comparable<App> {
	private String packageName;
	private String title;
	private String developer;
	private int ratings;
	private double score;
	private String price;
	private String description;
	private int downloads;
	private int fiveStars;
	private int fourStars;
	private int threeStars;
	private int twoStars;
	private int oneStar;
	private String newFeatures;
	private List<Review> reviews;
	
	public App(String packageName, String title, String developer, int ratings, double score, String price, String description, String downloads, int five, int four, int three, int two, int one, String newF) {
		super();
		this.packageName = packageName;
		this.title = title;
		this.developer = developer;
		this.ratings = ratings;
		this.score = score;
		this.price = price;
		this.description = description;
		this.downloads = parseDownloads(downloads);
		this.reviews = new ArrayList<Review>();
		this.fiveStars = five;
		this.fourStars = four;
		this.threeStars = three;
		this.twoStars = two;
		this.oneStar = one;
		this.newFeatures = newF;
	}
	
	private int parseDownloads(String downs) {
		return downs.equals("") ? 0 : Integer.parseInt(downs.replace("De ", "").split(" a ")[0].replaceAll(",", ""));
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public int getRatings() {
		return ratings;
	}

	public void setRatings(int ratings) {
		this.ratings = ratings;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getDownloads() {
		return downloads;
	}

	public void setDownloads(int downloads) {
		this.downloads = downloads;
	}

	public int getFiveStars() {
		return fiveStars;
	}

	public void setFiveStars(int fiveStars) {
		this.fiveStars = fiveStars;
	}

	public int getFourStars() {
		return fourStars;
	}

	public void setFourStars(int fourStars) {
		this.fourStars = fourStars;
	}

	public int getThreeStars() {
		return threeStars;
	}

	public void setThreeStars(int threeStars) {
		this.threeStars = threeStars;
	}

	public int getTwoStars() {
		return twoStars;
	}

	public void setTwoStars(int twoStars) {
		this.twoStars = twoStars;
	}

	public int getOneStar() {
		return oneStar;
	}

	public void setOneStar(int oneStar) {
		this.oneStar = oneStar;
	}

	public String getNewFeatures() {
		return newFeatures;
	}

	public void setNewFeatures(String newFeatures) {
		this.newFeatures = newFeatures;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void addReview(Review r) {
		reviews.add(r);
	}
	
	public void addReviews(List<Review> r) {
		reviews.addAll(r);
	}
	
	public void sortReviews() {
		Collections.sort(reviews);
	}
	
	public String toString() {
		return title + " by " + developer + " (" + score + ")\n" + ratings + " ratings\n" + description;
	}

	@Override
	public int compareTo(App arg0) {
		if(score > arg0.getScore()) {
			return 1;
		} else if(score < arg0.getScore()) {
			return -1;
		} else {
			if(downloads > arg0.getDownloads()) {
				return 1;
			} else if(downloads < arg0.getDownloads()) {
				return -1;
			} else {
				return 0;
			}
		}
	}
}
