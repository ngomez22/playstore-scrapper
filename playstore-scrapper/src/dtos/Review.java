package dtos;

import org.joda.time.DateTime;

public class Review implements Comparable<Review> {
	private String author;
	private int rating;
	private String content;
	private DateTime date;
	
	public Review(String author, int rating, String content, DateTime date) {
		super();
		this.author = author;
		this.rating = rating;
		this.content = content;
		this.date = date;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public int getRating() {
		return rating;
	}

	public void setRating(int rating) {
		this.rating = rating;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}
	
	public String toString() {
		return author + " (" + rating + "): " + content; 
	}

	@Override
	public int compareTo(Review r2) {
		if(date.isBefore(r2.getDate())) {
			return -1;
		} else if (date.isAfter(r2.getDate())) {
			return 1;
		} else {
			return 0;
		}
	}
}
